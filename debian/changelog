conda-package-handling (2.3.0-1) unstable; urgency=medium

  * New upstream version
    Closes: #1073387
  * Standards-Version: 4.7.0 (routine-update)

 -- Andreas Tille <tille@debian.org>  Tue, 25 Jun 2024 20:30:50 +0200

conda-package-handling (2.2.0-2) unstable; urgency=medium

  * Drop python3-six from (Build-)Depends
    Closes: #1052523
  * Build-Depends: s/dh-python/dh-sequence-python3/ (routine-update)

 -- Andreas Tille <tille@debian.org>  Fri, 08 Dec 2023 12:19:13 +0100

conda-package-handling (2.2.0-1) unstable; urgency=medium

  * New upstream version

 -- Andreas Tille <tille@debian.org>  Mon, 11 Sep 2023 12:30:15 +0200

conda-package-handling (2.1.0-1) unstable; urgency=medium

  * New upstream version
  * Standards-Version: 4.6.2 (routine-update)

 -- Andreas Tille <tille@debian.org>  Tue, 11 Jul 2023 17:44:04 +0200

conda-package-handling (2.0.1-2) unstable; urgency=medium

  * Team Upload.
  * Add patch to fix FTBFS on 32 bit archs
  * Stop mentioning removed files from d/copyright

 -- Nilesh Patra <nilesh@debian.org>  Sun, 01 Jan 2023 09:47:00 +0530

conda-package-handling (2.0.1-1) unstable; urgency=medium

  [ Andreas Tille ]
  * Team Upload.
  * Fix watch file
  * New upstream version

  [ Nilesh Patra ]
  * Deactivate test once and for all (Closes: #976506)

 -- Nilesh Patra <nilesh@debian.org>  Sat, 31 Dec 2022 18:16:18 +0530

conda-package-handling (1.8.1-1) unstable; urgency=medium

  * Team upload.
  * New upstream version 1.8.1
  * Bump Standards-Version to 4.6.1 (no changes needed)

 -- Nilesh Patra <nilesh@debian.org>  Sat, 28 May 2022 14:18:39 +0530

conda-package-handling (1.8.0-1) unstable; urgency=medium

  * New upstream version

 -- Andreas Tille <tille@debian.org>  Fri, 18 Mar 2022 12:54:38 +0100

conda-package-handling (1.7.3-2) unstable; urgency=medium

  * Build-Depends: python3-six
    Closes: #1002236
  * Fix watchfile to detect new versions on github
  * Standards-Version: 4.6.0 (routine-update)

 -- Andreas Tille <tille@debian.org>  Wed, 22 Dec 2021 11:22:34 +0100

conda-package-handling (1.7.3-1) unstable; urgency=medium

  * Team Upload.
  * Fix watch URL
  * New upstream version 1.7.3
  * Remove merged patch

 -- Nilesh Patra <nilesh@debian.org>  Sun, 15 Aug 2021 17:29:01 +0530

conda-package-handling (1.7.2-2) unstable; urgency=medium

  * Team upload.
  * Extend datetime to Dec 1, 2021 in api test
    to prevent failing tests in arm64. (Closes: #976506)
  * Standards-Version: 4.5.1 (routine-update)
  * Remove trailing whitespace in debian/changelog (routine-update)

 -- Nilesh Patra <npatra974@gmail.com>  Sun, 06 Dec 2020 20:59:39 +0530

conda-package-handling (1.7.2-1) unstable; urgency=medium

  * Team upload.
  * New upstream version
  * Add salsa-ci file (routine-update)
  * Rules-Requires-Root: no (routine-update)
  * Set upstream metadata fields: Bug-Submit.
  * Add autopkgtest
  * Add patch to remove regression in the new version

 -- Nilesh Patra <npatra974@gmail.com>  Tue, 27 Oct 2020 18:08:28 +0000

conda-package-handling (1.7.0-1) unstable; urgency=medium

  * New upstream version
    Closes: #963334
  * Standards-Version: 4.5.0 (routine-update)
  * debhelper-compat 13 (routine-update)

 -- Andreas Tille <tille@debian.org>  Mon, 22 Jun 2020 11:45:00 +0200

conda-package-handling (1.6.0-2) unstable; urgency=medium

  * Add missing copyrights
  * Set upstream metadata fields: Bug-Database, Repository, Repository-
    Browse.

 -- Andreas Tille <tille@debian.org>  Thu, 26 Dec 2019 08:22:21 +0100

conda-package-handling (1.6.0-1) unstable; urgency=medium

  * Initial release (Closes: #945148)

 -- Andreas Tille <tille@debian.org>  Wed, 20 Nov 2019 16:35:29 +0100
